package com.emids.business;

import com.emids.model.Patient;

public class BusinessRules {
	
	
	
	private static double getBasePremiumBasedOnAge(Patient patient){
		double basePremium = 5000;
		
		
		if(patient.getAge()>=18 ){
			
			basePremium = basePremium+basePremium*10/100;
		}
		
		 if(patient.getAge()>=25 ){
			basePremium = basePremium+basePremium*10/100;
		}
		
		if(patient.getAge()>=30){

			basePremium = basePremium+basePremium*10/100;
		}
		
		if(patient.getAge()>=35 ){
			basePremium = basePremium+basePremium*10/100;
		}
		
		if(patient.getAge()>=40){
			basePremium = basePremium+basePremium*20/100;
		}
		
		
		return basePremium;
	}
	
	private static double addGenderPrice(Patient patient,double amount){
		if(patient.getGender().equalsIgnoreCase("Male")){
			
			return amount*2/100; 
					
		}
		
		else{
			return 0.0;
		}
	}
	
	private static double addPreExistingConditions(Patient patient,double amount){
		
		int counter = 0;
		if(patient.getCurrentHealth().isBloodSugar()==true){
			counter=counter+1;
		}
		
		if(patient.getCurrentHealth().isHypertension()==true){
			counter=counter+1;
		}
		
		if(patient.getCurrentHealth().isBloodPressure()==true){
			counter=counter+1;
		}
		
		if(patient.getCurrentHealth().isOverweight()==true){
			counter=counter+1;
		}

		return amount*counter/100;
	}
	
	private static double addHabitsBasedPrice(Patient patient,double amount){
		int counter=0;
		if(patient.getHabits().isDailyExercise()==true){
			counter=counter-3;
		}
		
		if(patient.getHabits().isAlcohol()==true){
			counter=counter+3;

		}
		
		if(patient.getHabits().isDrugs()==true){
			counter=counter+3;
		}
		
		if(patient.getHabits().isSmoking()==true){
			counter=counter+3;
		}
		return amount*counter/100;
	}
	
	public static double generateHealthInsurancePremium(Patient patient){
		double amount = getBasePremiumBasedOnAge(patient);
		amount = amount+ addGenderPrice(patient, amount);
		amount = amount+addPreExistingConditions(patient,amount);
		amount = amount+addHabitsBasedPrice(patient,amount);
		
		return amount;
	}
}
