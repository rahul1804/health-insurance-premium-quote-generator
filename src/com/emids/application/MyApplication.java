package com.emids.application;

import com.emids.business.BusinessRules;
import com.emids.model.CurrentHealth;
import com.emids.model.Habits;
import com.emids.model.Patient;

public class MyApplication {

	public static void main(String[] args) {
		
		Patient patient = new Patient();
		CurrentHealth currentHealth = new CurrentHealth();
		Habits habits = new Habits();
		
		currentHealth.setHypertension(false);
		currentHealth.setBloodPressure(false);
		currentHealth.setBloodSugar(false);
		currentHealth.setOverweight(true);
		
		habits.setSmoking(false);
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		
		patient.setAge(34);
		patient.setGender("Male");
		patient.setName("Norman Gomes");
		patient.setCurrentHealth(currentHealth);
		patient.setHabits(habits);
		
		System.out.println("Health Insurance Premium for Mr. "+patient.getName()+":  "+ Math.round(BusinessRules.generateHealthInsurancePremium(patient)));

	}

}
