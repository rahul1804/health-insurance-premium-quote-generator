package com.emids.model;

public class Patient {
	
	private String name;
	private String gender;
	private Integer age;
	private CurrentHealth currentHealth;
	private Habits habits;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
	@Override
	public String toString() {
		return "Patient [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth=" + currentHealth
				+ ", habits=" + habits + "]";
	}
	
	
	

}
